<?php

use App\Http\Controllers\ImageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/**
 * Define routes for the ImageController actions.
 */
Route::get('/', [ImageController::class, 'index']);
Route::post('/images/upload', [ImageController::class, 'upload']);
Route::patch('/images/{id}/approve', [ImageController::class, 'approve']);
Route::patch('/images/{id}/reject', [ImageController::class, 'reject']);
