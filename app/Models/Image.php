<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    /**
     * Fillable attributes for the Image model.
     *
     * @var array
     */
    protected $fillable = [
        'filename',
        'status',
    ];
}
