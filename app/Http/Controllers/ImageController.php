<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Display the index page with all images.
     *
     * Retrieves all images from the database and renders the 'images.index' view.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $images = Image::all();
        return view('images.index', compact('images'));
    }

    /**
     * Upload and save an image from the request.
     *
     * Validates and processes the uploaded image, saves it to the 'images' directory,
     * creates a new Image model, and stores the filename in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time() . '.' . $request->image->extension();
        $request->image->move(public_path('images'), $imageName);

        $image = new Image(['filename' => $imageName]);
        $image->save();

        return response()->json(['success' => 'Image uploaded successfully']);
    }

    /**
     * Approve an image by updating its status to 'approved'.
     *
     * Finds the image by ID, updates the status, and saves the changes.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve($id)
    {
        $image = Image::findOrFail($id);
        $image->status = 'approved';
        $image->save();

        return response()->json(['success' => 'Image approved successfully']);
    }

    /**
     * Reject an image by updating its status to 'rejected'.
     *
     * Finds the image by ID, updates the status, and saves the changes.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function reject($id)
    {
        $image = Image::findOrFail($id);
        $image->status = 'rejected';
        $image->save();

        return response()->json(['success' => 'Image rejected successfully']);
    }
}
