<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
    <title>Images</title>
</head>
<body>
    <h1>Image Gallery</h1>

    <table border="1">
        <thead>
            <tr>
                <th>Image</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
            @foreach ($images as $image)
                <tr>
                    <td>
                        <img src="{{ asset('images/' . $image->filename) }}" alt="Image" width="100">
                    </td>
                    <td>{{ ucfirst($image->status) }}</td>
                    <td>
                        <button style="color: rgb(26, 163, 26); cursor: pointer;" onclick="approveImage({{ $image->id }})">Approve</button>
                        <button style="color: red; cursor: pointer;" onclick="rejectImage({{ $image->id }})">Reject</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <form action="{{ url('/images/upload') }}" method="post" enctype="multipart/form-data" id="uploadForm">
        @csrf
        <input type="file" name="image" id="image">
        <button type="submit">Upload Image</button>
    </form>

    <script>
        function approveImage(id) {
            axios.patch(`/images/${id}/approve`)
                .then(response => {
                    console.log(response.data);
                    location.reload();
                })
                .catch(error => {
                    console.error(error);
                });
        }

        function rejectImage(id) {
            axios.patch(`/images/${id}/reject`)
                .then(response => {
                    console.log(response.data);
                    location.reload();
                })
                .catch(error => {
                    console.error(error);
                });
        }

        document.getElementById('uploadForm').addEventListener('submit', function(event) {
            event.preventDefault();

            let formData = new FormData(this);

            axios.post('/images/upload', formData)
                .then(response => {
                    console.log(response.data);
                    location.reload();
                })
                .catch(error => {
                    console.error(error);
                });
        });
    </script>
</body>
</html>
