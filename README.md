
## Task

  upload images by javascript and display with ajax action approve or reject in db

## Solution

### Database Migration

- **Migration File:** 
  - Created "images" table with id, filename, status, and timestamps.

### ImageController

- **Methods:**
  - Managed image actions (upload, approve, reject).

### View (images.index.blade.php)

- **Features:**
  - Displayed an interactive gallery.
  - Included upload, approve, and reject functionalities.

### JavaScript

- **Library:**
  - Utilized Axios for seamless AJAX actions.
